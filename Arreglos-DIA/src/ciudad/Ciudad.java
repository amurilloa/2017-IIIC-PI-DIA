/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciudad;

/**
 *
 * @author ALLAN
 */
public class Ciudad {

    private String nombre;
    private int tarifa;

    public Ciudad() {
    }

    public Ciudad(String nombre, int tarifa) {
        this.nombre = nombre;
        this.tarifa = tarifa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    @Override
    public String toString() {
        return "Ciudad{" + "nombre=" + nombre + ", tarifa=" + tarifa + '}';
    }
}
