/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciudad;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {
        Logica log = new Logica();
        System.out.println("Con 1250 puedo ir a: \n" + log.puedeViajar(1250));

        System.out.println("Ciudad con mayor tarifa: "
                + log.mayorTarifa().getNombre());
        
        Ciudad mayor = log.mayorTarifa();
        System.out.println("Ciudad con mayor tarifa: "
                + mayor.getNombre() + "-"
                + mayor.getTarifa());
       
    }
}
