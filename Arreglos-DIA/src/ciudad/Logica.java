/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciudad;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Ciudad[] ciudades;

    public Logica() {
        ciudades = new Ciudad[10];
        cargarCiudades();
    }

    private void cargarCiudades() {
        ciudades[0] = new Ciudad("Ciudad Quesada", 1200);
        ciudades[1] = new Ciudad("Fortuna", 1000);
        ciudades[2] = new Ciudad("San Jose", 2200);
        ciudades[3] = new Ciudad("Limon", 2500);
        ciudades[4] = new Ciudad("Alajuela", 1800);
        ciudades[5] = new Ciudad("San Ramon", 1600);
        ciudades[6] = new Ciudad("Puntarenas", 2000);
        ciudades[7] = new Ciudad("Jaco", 2100);
        ciudades[8] = new Ciudad("Chachagua", 1050);
        ciudades[9] = new Ciudad("Puerto Viejo", 1250);
    }

    public String puedeViajar(int monto) {
        String permitidas = "";

        for (int i = 0; i < ciudades.length; i++) {
            if (ciudades[i].getTarifa() <= monto) {
                permitidas += ciudades[i].getNombre() + "\n";
            }
        }
        return permitidas;
    }

    public Ciudad mayorTarifa() {
        Ciudad mayor = ciudades[0];
        for (int i = 0; i < ciudades.length; i++) {
            if (ciudades[i].getTarifa() > mayor.getTarifa()) {
                mayor = ciudades[i];
            }
        }
        return mayor;
    }

}
