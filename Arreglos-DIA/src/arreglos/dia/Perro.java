/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos.dia;

/**
 *
 * @author ALLAN
 */
public class Perro {

    private int pedegree;
    private String nombre;
    private String raza;
    private String propietario;

    public Perro() {
    }

    public Perro(int pedegree, String nombre, String raza, String propietario) {
        this.pedegree = pedegree;
        this.nombre = nombre;
        this.raza = raza;
        this.propietario = propietario;
    }

    public int getPedegree() {
        return pedegree;
    }

    public void setPedegree(int pedegree) {
        this.pedegree = pedegree;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    @Override
    public String toString() {
        return "Perro{" + "pedegree=" + pedegree + ", nombre=" + nombre + ", raza=" + raza + ", propietario=" + propietario + '}';
    }
    
}
