/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos.dia;

/**
 *
 * @author ALLAN
 */
public class Logica {

    /**
     * Calcula el promedio de los valores de una arreglo
     *
     * @param arreglo Arreglo de valores
     * @return double con promedio calculado
     */
    public double promedio(int[] arreglo) {
        int total = 0;
        for (int cont = 0; cont < arreglo.length; cont++) {
            total += arreglo[cont];
        }
        return (double) total / arreglo.length;
    }

    /**
     *
     * @param arreglo
     * @return
     */
    public int mayor(int[] arreglo) {
        int mayor = arreglo[0];
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] > mayor) {
                mayor = arreglo[i];
            }
        }
        return mayor;
    }

    /**
     *
     * @param arreglo
     * @return
     */
    public int sumaImpares(int[] arreglo) {
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                suma += arreglo[i];
            }
        }
        return suma;
    }

    /**
     *
     * @param arreglo
     * @return
     */
    public String imprimir(int[] arreglo) {
        String mensaje = "";
        for (int i = 0; i < arreglo.length; i++) {
            mensaje += arreglo[i] + " ";
        }
        return mensaje;
    }

    /**
     *
     * @param arreglo
     * @param numero
     * @return
     */
    public boolean buscar(int[] arreglo, int numero) {
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == numero) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param arreglo
     * @param numero
     * @return
     */
    public int buscarPos(int[] arreglo, int numero) {
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == numero) {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * @param arreglo
     * @return
     */
    public int[] invertir(int[] arreglo) {
        int[] invertido = new int[arreglo.length];
        int x = arreglo.length - 1;
        for (int i = 0; i < invertido.length; i++) {
            invertido[i] = arreglo[x--];
        }
        return invertido;
    }

    /**
     * 
     * @param arreglo 
     */
    public void rotar(int[] arreglo) {
        int ult = arreglo[arreglo.length - 1];
        for (int i = arreglo.length - 1; i > 0; i--) {
            arreglo[i] = arreglo[i - 1];
        }
        arreglo[0] = ult;
    }

}
