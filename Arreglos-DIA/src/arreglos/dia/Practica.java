/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos.dia;

/**
 *
 * @author ALLAN
 */
public class Practica {

    public static void main(String[] args) {
        int[] numeros = {1, 3, 5, 4, 6, 2};
        Logica log = new Logica();
        System.out.println("Arreglo: " + log.imprimir(numeros));
        System.out.println("Promedio: " + log.promedio(numeros));
        System.out.println("\u2211 Impares: " + log.sumaImpares(numeros));
        System.out.println("Buscar un 7: " + log.buscar(numeros, 7));
        System.out.println("Buscar un 4: " + log.buscar(numeros, 4));
        System.out.println("Buscar un 7: " + log.buscarPos(numeros, 7));
        System.out.println("Buscar un 4: " + log.buscarPos(numeros, 4));
        int[] temp = log.invertir(numeros);
        System.out.println("Invertir: " + log.imprimir(temp));
        log.rotar(numeros);
        System.out.println("Rotar: " + log.imprimir(numeros));
        
    }
}
