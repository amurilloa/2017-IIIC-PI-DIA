/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos.dia;

/**
 *
 * @author ALLAN
 */
public class ArreglosDIA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        int[] arreglo = new int[Util.leerInt("Tamaño del arreglo")];
        int[] arreglo = {1, 8, 2, 15, 13, 4};
        // |1|2|3|4|5| Cantidad de espacios 
//      // |0|1|2|3|4| Indices inician en 0
        System.out.println("Largo del Arreglo: " + arreglo.length);
        arreglo[0] = 10;
        int i = 1;
        arreglo[i] = 20;
        System.out.println("0- " + arreglo[0]);
        System.out.println(i + "- " + arreglo[i]);
        System.out.println(arreglo[0] + arreglo[i]);
        System.out.println("Ultimo valor: " + arreglo[arreglo.length - 1]);
        System.out.println("Ultimo indice: " + (arreglo.length - 1));

        String[] dias = new String[7];
        String[] diasDos = {"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"};
        dias[0] = "Domingo";
        dias[1] = "Lunes";
        dias[2] = "Martes";
        dias[3] = "Miércoles";
        dias[4] = "Jueves";
        dias[5] = "Viernes";
        dias[6] = "Sábado";

        arreglo[3] = 10;
        int x = arreglo[3];

        for (int j = 0; j < arreglo.length; j++) {
            System.out.print(arreglo[j] + " ");
        }

        System.out.println("");
        Prueba p = new Prueba();
        int[] temp = p.ordenar(arreglo);

        Perro[] perros = new Perro[10];
        perros[0] = new Perro(1, "Capitan", "Pastor", "Allan Murillo");
        Perro pe = new Perro(2, "Max", "Desconocido", "Lineth Matamoros");
        perros[1] = pe;

        for (int j = 0; j < perros.length; j++) {
            if (perros[j] == null) {
                Perro aux = new Perro();
                //aux.setNombre(Util.leerString("Digite el nombre del perro"));
                System.out.println(aux);
                perros[j] = aux;
            }
        }

        //Suma de valores
        int[] numeros = {1, 3, 5, 6, 3, 2};

        int total = 0;
        for (int cont = 0; cont < numeros.length; cont++) {
            total += numeros[cont];
        }
        System.out.println("Total: " + total);
        System.out.println("Promedio: " + ((double) total / numeros.length));
        //Encontrar el menor 
        //Encontrar el mayor
        
        Logica log = new Logica();
        System.out.println(log.promedio(numeros));
    }
}
