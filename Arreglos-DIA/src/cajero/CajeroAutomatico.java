/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajero;

/**
 *
 * @author ALLAN
 */
public class CajeroAutomatico {

    private int saldo;
    private int[] billetes;
    private int[] dinero;

    public CajeroAutomatico() {
        saldo = 1000;
        billetes = new int[]{10000, 5000, 2000, 1000, 500, 100};
        dinero = new int[6];
    }

    public int getSaldo() {
        return saldo;
    }

    /**
     * 
     * @param monto 
     */
    public void deposito(int monto) {
        saldo += monto;
    }

    /**
     * 
     * @param cantidad
     * @return 
     */
    public boolean retiro(int cantidad) {
        if (saldo >= cantidad) {
            saldo -= cantidad;
            desgloseDinero(cantidad);
            return true;
        }
        return false;
    }

    /**
     * Calcula el desglose de dinero de una cantidad 
     * @param cantidad int cantidad a retirar 
     */
    public void desgloseDinero(int cantidad) {
        for (int i = 0; i < billetes.length; i++) {
            if (cantidad >= billetes[i]) {
                dinero[i] = cantidad / billetes[i];
                cantidad %= billetes[i];
            }
        }
    }

    @Override
    public String toString() {
        String mensaje = "Dinero retirado: \n";
        for (int i = 0; i < dinero.length; i++) {
            if (dinero[i] > 0) {
                mensaje += dinero[i] + " billetes de " + billetes[i] + "\n";
            }
        }
        mensaje += "Saldo de la cuenta es: " + saldo;
        return mensaje;
    }

}
