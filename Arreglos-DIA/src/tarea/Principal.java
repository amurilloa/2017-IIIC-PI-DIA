/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import arreglos.dia.Util;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {
        Encuesta e = new Encuesta();

        e.registrar(new Estudiante(1, 1, 1, 100000));
        e.registrar(new Estudiante(2, 2, 2, 0));
        e.registrar(new Estudiante(3, 2, 2, 0));
        e.registrar(new Estudiante(4, 1, 1, 120000));
        e.registrar(new Estudiante(5, 2, 1, 140000));
        e.registrar(new Estudiante(6, 1, 2, 0));
        e.registrar(new Estudiante(7, 2, 2, 0));
        e.registrar(new Estudiante(8, 2, 1, 200000));
        e.registrar(new Estudiante(9, 2, 2, 0));

        //Realizar la encuesta
        String menu = "\n   MENU PRINCIPAL\n"
                + "1. Entrevistar Estudiante\n"
                + "2. Finalizar encuesta\n"
                + "3. Salir\n"
                + "Seleccione una opción";

        String menuA = "\n   MENU PRINCIPAL\n"
                + "1. Hombres\n"
                + "2. Mujeres\n"
                + "3. Hombres que trabajan y sueldo promedio\n"
                + "4. Mujeres que trabajan y sueldo promedio\n"
                + "5. Volver\n"
                + "Seleccione una opción";

        while (true) {
            int op = Util.leerInt(menu);
            if (op == 3) {
                break;
            } else if (op == 1) {
                Estudiante es = new Estudiante();
                es.setCedula(Util.leerInt("Cédula"));
                es.setSexo(Util.leerInt("Sexo: \n1. Masculino\n2.Femenino"));
                es.setTrabaja(Util.leerInt("Trabaja:\n1.Si\n2.No"));
                if (es.getTrabaja() == 1) {
                    es.setSueldo(Util.leerInt("Sueldo"));
                }
                e.registrar(es);
            } else if (op == 2) {
                //Analizar la encuesta
                while (true) {
                    op = Util.leerInt(menuA);
                    if (op == 5) {
                        break;
                    } else if (op == 1) {
                        System.out.println("Porcentaje de Hombres: " + e.porcentaje(1));
                    } else if (op == 2) {
                        System.out.println("Porcentaje de Mujeres: " + e.porcentaje(2));
                    } else if (op == 3) {
                        System.out.println("Hombres que trabajan:\n" + e.porcentajeT(1));
                    } else if (op == 4) {
                        System.out.println("Mujeres que trabajan:\n" + e.porcentajeT(2));
                    }
                }
            } else if (op == 12) {//Ver encuesta
                System.out.println(e.imprimir());
            }
        }

    }
}
