/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Estudiante {

    private int cedula;
    private int sexo;
    private int trabaja;
    private int sueldo;

    public Estudiante() {
    }

    public Estudiante(int cedula, int sexo, int trabaja, int sueldo) {
        this.cedula = cedula;
        this.sexo = sexo;
        this.trabaja = trabaja;
        this.sueldo = sueldo;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public int getTrabaja() {
        return trabaja;
    }

    public void setTrabaja(int trabaja) {
        this.trabaja = trabaja;
    }

    public int getSueldo() {
        return sueldo;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "cedula=" + cedula + ", sexo=" + sexo + ", trabaja=" + trabaja + ", sueldo=" + sueldo + '}';
    }
}
