/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import arreglos.dia.Util;

/**
 *
 * @author ALLAN
 */
public class Encuesta {

    private Estudiante[] estudiantes;
    private int cantEstu;

    public Encuesta() {
        estudiantes = new Estudiante[50];
    }

    /**
     *
     * @param e
     * @return
     */
    public boolean registrar(Estudiante e) {
        if (cantEstu >= 50) {
            return false;
        }

        estudiantes[cantEstu++] = e;
        return true;
    }

    /**
     *
     * @param e
     * @return
     */
    public boolean registrarDos(Estudiante e) {
        for (int i = 0; i < estudiantes.length; i++) {
            if (estudiantes[i] == null) {
                estudiantes[i] = e;
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param sexo
     * @return
     */
    public double porcentaje(int sexo) {
        int cant = 0;
        int total = 0;
        for (int i = 0; i < estudiantes.length; i++) {
            if (estudiantes[i] != null) {
                if (estudiantes[i].getSexo() == sexo) {
                    cant++;
                }
                total++;
            }
        }
        return Util.redondear(cant * 100 / (double) total);
    }

    public String porcentajeT(int sexo) {
        int cant = 0;
        int total = 0;
        double promedio = 0;
        for (int i = 0; i < estudiantes.length; i++) {
            if (estudiantes[i] != null) {
                if (estudiantes[i].getSexo() == sexo) {
                    if (estudiantes[i].getTrabaja() == 1) {
                        cant++;
                        promedio += estudiantes[i].getSueldo();
                    }
                    total++;
                }
            }
        }
        promedio /= cant;
        double porcentaje = cant * 100 / (double) total;
        return "Porcentaje: " + Util.redondear(porcentaje)
                + "\nSueldo Promedio: " + Util.redondear(promedio);
    }

    public Estudiante[] getEstudiantes() {
        return estudiantes;
    }

    /**
     *
     * @return
     */
    public String imprimir() {
        String mensaje = "";

        for (int i = 0; i < estudiantes.length; i++) {
            if (estudiantes[i] != null) {
                mensaje += (i + 1) + "." + estudiantes[i] + "\n";
            }
        }
        return mensaje;
    }
}
