/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoov2.dia;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Rectangulo {
    
    private int base;
    private int altura;
    private boolean relleno;
    private Color color;
    private int x;
    private int y;

    public Rectangulo() {
    }

    public Rectangulo(int base, int altura, boolean relleno, Color color, int x, int y) {
        this.base = base;
        this.altura = altura;
        this.relleno = relleno;
        this.color = color;
        this.x = x;
        this.y = y;
    }
    
    public void paint(Graphics g) {
        g.setColor(color);
        if (relleno) {
            g.fillRect(x, y, base, altura);
        } else {
            g.drawRect(x, y, base, altura);
        }
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public boolean isRelleno() {
        return relleno;
    }

    public void setRelleno(boolean relleno) {
        this.relleno = relleno;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Rectangulo{" + "base=" + base + ", altura=" + altura + ", relleno=" + relleno + ", color=" + color + ", x=" + x + ", y=" + y + '}';
    }
}
