/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoov2.dia;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Cuadrado {

    private int lado;
    private boolean relleno;
    private Color color;
    private int x;
    private int y;

    public Cuadrado() {
    }

    public Cuadrado(int lado, boolean relleno, Color color, int x, int y) {
        this.lado = lado;
        this.relleno = relleno;
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public double getArea() {
        return Math.pow(lado, 2);
    }

    public void paint(Graphics g) {
        g.setColor(color);
        if (relleno) {
            g.fillRect(x, y, lado, lado);
        } else {
            g.drawRect(x, y, lado, lado);
        }
    }

    public int getLado() {
        return lado;
    }

    public void setLado(int lado) {
        this.lado = lado;
    }

    public boolean isRelleno() {
        return relleno;
    }

    public void setRelleno(boolean relleno) {
        this.relleno = relleno;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Cuadrado{" + "lado=" + lado + ", relleno=" + relleno + ", color=" + color + ", x=" + x + ", y=" + y + '}';
    }
}
