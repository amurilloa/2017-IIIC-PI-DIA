/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoov2.dia;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class PanelPrincipal extends JPanel implements KeyListener {

    private Rectangulo fondo;
    private Cuadrado c1;
    private Triangulo t1;
    private Color cc;
    private int cX;
    private int fig;

    public PanelPrincipal() {
        //Tamaño al panel
        setPreferredSize(new Dimension(400, 400));
        //Borde 
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 8));
        addKeyListener(this);
        setFocusable(true);
        fondo = new Rectangulo(384, 384, true, Color.GRAY, 8, 8);
        c1 = new Cuadrado(75, true, Color.BLUE, 100, 100);
        t1 = new Triangulo(75, 100, true, Color.CYAN, 250, 50);
        cc = Color.GREEN;
        cX = 200;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Circulo cir = new Circulo(50, true, cc, cX, 200);
        fondo.paint(g);
        c1.paint(g);
        t1.paint(g);
        cir.paint(g);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
//        System.out.println(ke.getKeyChar()); // == 'a' == 'A'
//        System.out.println(ke.getKeyCode()); // == VK_A //No sirve el el Typed
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        cambiarFondo(ke);
        if (ke.getKeyCode() == KeyEvent.VK_6) {
            if (fig == KeyEvent.VK_R) {
                c1.setColor(Color.GREEN);
            } else if (fig == KeyEvent.VK_C) {
                cc = Color.GREEN;
            } else if (fig == KeyEvent.VK_T) {
                t1.setColor(Color.GREEN);
            }
        } else if (ke.getKeyCode() == KeyEvent.VK_7) {
            if (fig == KeyEvent.VK_R) {
                c1.setColor(Color.YELLOW);
            } else if (fig == KeyEvent.VK_C) {
                cc = Color.YELLOW;
            } else if (fig == KeyEvent.VK_T) {
                t1.setColor(Color.YELLOW);
            }
        } else if (ke.getKeyCode() == KeyEvent.VK_8) {
            if (fig == KeyEvent.VK_R) {
                c1.setColor(Color.BLACK);
            } else if (fig == KeyEvent.VK_C) {
                cc = Color.BLACK;
            } else if (fig == KeyEvent.VK_T) {
                t1.setColor(Color.BLACK);
            }
        } else if (ke.getKeyCode() == KeyEvent.VK_9) {
            if (fig == KeyEvent.VK_R) {
                c1.setColor(Color.CYAN);
            } else if (fig == KeyEvent.VK_C) {
                cc = Color.CYAN;
            } else if (fig == KeyEvent.VK_T) {
                t1.setColor(Color.CYAN);
            }
        } else if (ke.getKeyCode() == KeyEvent.VK_0) {
            if (fig == KeyEvent.VK_R) {
                c1.setColor(Color.WHITE);
            } else if (fig == KeyEvent.VK_C) {
                cc = Color.WHITE;
            } else if (fig == KeyEvent.VK_T) {
                t1.setColor(Color.WHITE);
            }
        } else if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            t1.setX(t1.getX() - 1);
            cX--;
        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            t1.setX(t1.getX() + 1);
            cX++;
        } else if (ke.getKeyCode() == KeyEvent.VK_UP) {
            t1.setY(t1.getY() - 1);
        } else if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            t1.setY(t1.getY() + 1);
        } else {
            fig = ke.getKeyCode();
        }
        repaint();

    }

    private void cambiarFondo(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_1) {
            fondo.setColor(Color.GRAY);
        } else if (ke.getKeyCode() == KeyEvent.VK_2) {
            fondo.setColor(Color.MAGENTA);
        } else if (ke.getKeyCode() == KeyEvent.VK_3) {
            fondo.setColor(Color.RED);
        } else if (ke.getKeyCode() == KeyEvent.VK_4) {
            fondo.setColor(Color.CYAN);
        } else if (ke.getKeyCode() == KeyEvent.VK_5) {
            fondo.setColor(Color.BLUE);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

    /*
        Cambiar el fondo con los números 1-5
        Seleccionar una figura y cabiar de color con los números del 6-0
        T+6-0: Triangulo  
        C+6-0: Circulo
        R+6-0: Cuadrado
     */
}
