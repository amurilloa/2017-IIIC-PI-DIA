/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoov2.dia;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Circulo {

    private int radio;
    private boolean relleno;
    private Color color;
    private int x;
    private int y;

    public Circulo() {
    }

    public Circulo(int radio, boolean relleno, Color color, int x, int y) {
        this.radio = radio;
        this.relleno = relleno;
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        if (relleno) {
            g.fillOval(x, y, radio * 2, radio * 2);
        } else {
            g.drawOval(x, y, radio * 2, radio * 2);
        }
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public boolean isRelleno() {
        return relleno;
    }

    public void setRelleno(boolean relleno) {
        this.relleno = relleno;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Circulo{" + "radio=" + radio + ", relleno=" + relleno + ", color=" + color + ", x=" + x + ", y=" + y + '}';
    }
}
