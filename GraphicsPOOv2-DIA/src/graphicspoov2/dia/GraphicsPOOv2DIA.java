/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoov2.dia;

import java.awt.Dimension;
import javax.swing.JFrame;

/**
 *
 * @author ALLAN
 */
public class GraphicsPOOv2DIA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //JFrame
        JFrame frm = new JFrame("Demo - Graphics v2");
        //Panel y agregarlo
        frm.add(new PanelPrincipal());
        frm.pack();
        //Propiedades del formulario
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Cierre la aplicación
        frm.setLocationRelativeTo(null); //Centrar el formulario
        //Hacer visible el formulario
        frm.setVisible(true);
    }

}
