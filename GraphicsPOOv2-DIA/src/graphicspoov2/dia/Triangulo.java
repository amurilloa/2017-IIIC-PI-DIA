/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoov2.dia;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Triangulo {

    private int base;
    private int altura;
    private boolean relleno;
    private Color color;
    private int x;
    private int y;

    public Triangulo() {
    }

    public Triangulo(int base, int altura, boolean relleno, Color color, int x, int y) {
        this.base = base;
        this.altura = altura;
        this.relleno = relleno;
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics g) {
        int[] xs = {x + base / 2, x, x + base};
        int[] ys = {y, y + altura, y + altura};

        g.setColor(color);
        if (relleno) {
            g.fillPolygon(xs, ys, 3);
        } else {
            g.drawPolygon(xs, ys, 3);
        }
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public boolean isRelleno() {
        return relleno;
    }

    public void setRelleno(boolean relleno) {
        this.relleno = relleno;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Triangulo{" + "base=" + base + ", altura=" + altura + ", relleno=" + relleno + ", color=" + color + ", x=" + x + ", y=" + y + '}';
    }

}
