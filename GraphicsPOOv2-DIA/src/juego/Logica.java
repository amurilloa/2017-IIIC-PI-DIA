/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Propiedad[] propiedades;

    public Logica() {
        propiedades = new Propiedad[40];
        configurar();
    }

    private void configurar() {
        propiedades[0] = new Propiedad("SALIDA", 0, -200, true, Color.WHITE, 'i');
        propiedades[1] = new Propiedad("Talamanca", 50, 5, false, new Color(127, 110, 154), 'n');
        propiedades[2] = new Propiedad("SORPRESA", 0, 0, true, Color.CYAN, 's');
        propiedades[3] = new Propiedad("Isla del Coco", 50, 10, false, new Color(127, 110, 154), 'n');
        propiedades[4] = new Propiedad("IMP DE UTILIDADES", 0, 200, true, Color.WHITE, 'n');
        propiedades[5] = new Propiedad("Aerovías del Pacífico", 0, 25, true, Color.ORANGE, 'n');
        propiedades[6] = new Propiedad("Cañas", 50, 15, false, Color.CYAN, 'n');
        propiedades[7] = new Propiedad("LOTERIA", 0, 0, true, Color.YELLOW, 'l');
    }

    public void pintarTablero(Graphics g) {
        int x = 15;
        int y = 15;
        int linea = 0;
        for (int i = 0; i < propiedades.length; i++) {
            if (propiedades[i] != null) {
                propiedades[i].pintar(g, x, y);
                if (propiedades[i].getTipo() == 'i') {
                    x += 80;
                } else {
                    x += 60;
                }
            }
        }
    }

}
