/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class PanelPrincipal extends JPanel implements KeyListener {

    private Cuadrado c1;
    private Logica log;
    public PanelPrincipal() {
        //Tamaño al panel
        setPreferredSize(new Dimension(600, 600));
        //Borde 
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 6));
        addKeyListener(this);
        setFocusable(true);
        log = new Logica();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        log.pintarTablero(g);
        
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            c1.setX(c1.getX() - 2);
        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            c1.setX(c1.getX() + 2);
        } else if (ke.getKeyCode() == KeyEvent.VK_UP) {
            c1.setY(c1.getY() - 2);
        } else if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            c1.setY(c1.getY() + 2);
        }
        repaint();

    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }
}
