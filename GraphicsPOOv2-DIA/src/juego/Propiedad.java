/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Propiedad {

    private String nombre;
    private int precio;
    private int peaje;
    private boolean vendido;
    private Color color;
    private char tipo;
    //i: salida
    //c: carcel, 
    //p: parqueo, 
    //v: vaya a la carcel , 
    //l: loteria, 
    //s: sorpresa, 
    //n: normal 

    public Propiedad() {
    }

    public Propiedad(String nombre, int precio, int peaje, boolean vendido, Color color, char tipo) {
        this.nombre = nombre;
        this.precio = precio;
        this.peaje = peaje;
        this.vendido = vendido;
        this.color = color;
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getPeaje() {
        return peaje;
    }

    public void setPeaje(int peaje) {
        this.peaje = peaje;
    }

    public boolean isVendido() {
        return vendido;
    }

    public void setVendido(boolean vendido) {
        this.vendido = vendido;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Propiedad{" + "nombre=" + nombre + ", precio=" + precio + ", peaje=" + peaje + ", vendido=" + vendido + ", color=" + color + ", tipo=" + tipo + '}';
    }

    public void pintar(Graphics g, int x, int y) {
        if (tipo == 'i') {
            g.setColor(color);
            g.fillRect(x, y, 85, 85);
            
            g.setColor(Color.BLACK);
            g.drawRect(x, y, 85, 85);
            

        } else if (tipo == 'n') {
            g.setColor(Color.BLACK);
            g.drawRect(x, y, 60, 85);

            g.setColor(color);
            g.fillRect(x + 1, y + 1, 59, 24);

            g.setColor(Color.BLACK);
            g.drawString(nombre, x + 5, y + 15);
            g.drawLine(x, y + 25, x + 60, y + 25);

            g.setColor(Color.WHITE);
            g.fillRect(x + 1, y + 26, 59, 59);
        }

    }

}
