/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics.dia;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Circulo {

    private int x;
    private int y;
    private int radio;
    private Color color;
    private boolean esRelleno;

    public Circulo() {
        radio = 20;
        color = Color.BLACK;
        esRelleno = true;
    }

    public Circulo(int x, int y, int radio, Color color, boolean esRelleno) {
        this.x = x;
        this.y = y;
        this.radio = radio;
        this.color = color;
        this.esRelleno = esRelleno;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        if (esRelleno) {
            g.fillOval(x, y, radio, radio);
        } else {
            g.drawOval(x, y, radio, radio);
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isEsRelleno() {
        return esRelleno;
    }

    public void setEsRelleno(boolean esRelleno) {
        this.esRelleno = esRelleno;
    }

    @Override
    public String toString() {
        return "Circulo{" + "x=" + x + ", y=" + y + ", radio=" + radio + ", color=" + color + ", esRelleno=" + esRelleno + '}';
    }

}
