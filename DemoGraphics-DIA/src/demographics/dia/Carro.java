package demographics.dia;

import java.awt.Color;
import java.awt.Graphics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ALLAN
 */
public class Carro {

    private int direccion; //0:DER, 1:IZQ
    private Color colorP;
    private Color colorS;
    private Color colorO;
    private int tamano;
    private boolean estaEncendido;
    private boolean lucesEncendidas;
    private int x;
    private int y;

    public Carro() {
        colorP = Color.RED;
        colorS = Color.BLACK;
        colorO = Color.BLACK;
        tamano = 1;
    }

    public Carro(int tamano, int x, int y) {
        this.tamano = tamano;
        this.x = x;
        this.y = y;
        colorP = Color.BLACK;
    }

    public Carro(int direccion, Color colorP, Color colorS, Color colorO, int tamano, boolean estaEncendido, boolean lucesEncendidas) {
        this.direccion = direccion;
        this.colorP = colorP;
        this.colorS = colorS;
        this.colorO = colorO;
        this.tamano = tamano;
        this.estaEncendido = estaEncendido;
        this.lucesEncendidas = lucesEncendidas;
    }

    public void pintar(Graphics g) {

        g.setColor(colorP);
        //Correcería
        int[] xs = {50, 80, 100, 130, 130, 10, 10, 50};
        if (direccion == 1) {
            xs = new int[]{50, 80, 100, 130, 130, 10, 10, 50};

        }
        int[] ys = {10, 10, 30, 35, 55, 55, 30, 30};

        for (int i = 0; i < xs.length; i++) {
            xs[i] *= tamano;
            xs[i] += x;

            ys[i] *= tamano;
            ys[i] += y;
        }

        g.fillPolygon(xs, ys, xs.length);

        //Bombillo
        g.setColor(Color.yellow);
        g.fillRect(tamano * 125 + x, tamano * 36 + y, 4 * tamano, 4 * tamano);
        g.setColor(Color.red);
        g.fillRect(tamano * 11 + x, tamano * 33 + y, 4 * tamano, 7 * tamano);

        //Llantas
        g.setColor(Color.GRAY);
        g.fillOval(tamano * 100 + x, tamano * 45 + y, tamano * 20, tamano * 20);
        g.fillOval(tamano * 20 + x, tamano * 45 + y, tamano * 20, tamano * 20);

        //Bumpers
        g.setColor(Color.GRAY);
        g.fillRect(tamano * 130 + x, tamano * 51 + y, tamano * 4, tamano * 4);
        g.fillRect(tamano * 6 + x, tamano * 51 + y, tamano * 4, tamano * 4);
    }

    private int[] invertir(int[] arreglo) {
        int[] aux = new int[arreglo.length];
        for (int i = 0, j = aux.length - 1; i < arreglo.length; i++, j--) {
            aux[i] = arreglo[j];
        }
        return aux;
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

    public Color getColorP() {
        return colorP;
    }

    public void setColorP(Color colorP) {
        this.colorP = colorP;
    }

    public Color getColorS() {
        return colorS;
    }

    public void setColorS(Color colorS) {
        this.colorS = colorS;
    }

    public Color getColorO() {
        return colorO;
    }

    public void setColorO(Color colorO) {
        this.colorO = colorO;
    }

    public int getTamano() {
        return tamano;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }

    public boolean isEstaEncendido() {
        return estaEncendido;
    }

    public void setEstaEncendido(boolean estaEncendido) {
        this.estaEncendido = estaEncendido;
    }

    public boolean isLucesEncendidas() {
        return lucesEncendidas;
    }

    public void setLucesEncendidas(boolean lucesEncendidas) {
        this.lucesEncendidas = lucesEncendidas;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
