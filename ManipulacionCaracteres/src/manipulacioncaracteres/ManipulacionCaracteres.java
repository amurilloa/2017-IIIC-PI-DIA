/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacioncaracteres;

/**
 *
 * @author ALLAN
 */
public class ManipulacionCaracteres {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();
        //|0|1|2|3|4|5|6|7|8|9|
        //|h|o|l|a| |m|u|n|d|o|
        String texto = "hola mundo";
        texto = "Hola Mundo";
        //Largo de un texto
        System.out.println("Largo: " + texto.length());
        //Obtener una letra o caracter en una posición
        System.out.println("Obtener una letra en x posición: " + texto.charAt(5));
        String var1 = "hola";
        String var2 = "Hola";
        System.out.println("Variables ");
        System.out.println("\tvar1: " + var1);
        System.out.println("\tvar2: " + var2);
        System.out.println("\tEquals: " + var1.equals(var2));
        System.out.println("\tEquals Ignore Case: " + var1.equalsIgnoreCase(var2));

        //Valor número de una letra y viceversa
        System.out.println((int) texto.charAt(5));
        System.out.println((char) 77);

        //Invertir una frase: hola mundo = odnum aloh
//        "a"+"b"="ab"
//        "b"+"a"="ba"
        System.out.println(log.invertir(texto));
        
        texto = "HoLa MuNdO";
        //"3 ARROZ MONTES"
        //"5 aZuCar MoReNo"

        System.out.println(texto);
        System.out.println("Mayúscula: " + texto.toUpperCase());
        System.out.println("Minúscula: " + texto.toLowerCase());
        
        texto = "Hola MuÑdÓ";

        //Contar la cantidad de mayusculas de un texto
        System.out.println("Cant. Mayusculas: " + log.cantMayusculas(texto));
        
        texto = "Hola Mundo";
        System.out.println(texto.replace("a m", "M"));
        System.out.println(texto.replaceAll("[^a-z]", "-"));
        
        System.out.println("Cant. Vocales: " + log.canVocales("aurelio"));
        System.out.println("Cant. Espacios: " + log.canEspacios("ho la mun do"));
        System.out.println("Elim. Espacios: " + log.eliEspacios("ho la mun do"));
        System.out.println("Primera Letra: " + log.primeraLetra("ho la mun do"));
    }
    
}
