/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacioncaracteres;

/**
 *
 * @author ALLAN
 */
public class Logica {

    /**
     *
     * @param texto
     * @return
     */
    public String invertir(String texto) {
        String nuevo = "";
        for (int i = 0; i < texto.length(); i++) {
            nuevo = texto.charAt(i) + nuevo;
        }
        return nuevo;
    }

    /**
     *
     * @param texto
     * @return
     */
    public int cantMayusculas(String texto) {
        int can = 0;
        for (int i = 0; i < texto.length(); i++) {
            if (Character.isUpperCase(texto.charAt(i))) {
                can++;
            }
        }
        return can;
    }

    /**
     * Cuenta la cantidad de vocales que aparecen en un texto
     *
     * @param texto que desea evaluar
     * @return int cantidad de vocales
     */
    public int canVocales(String texto) {
        return texto.toUpperCase().replaceAll("[^AEOIU]", "").length();
    }

    /**
     * Cuenta la cantidad de espacios que aparecen en un texto
     *
     * @param texto que desea evaluar
     * @return int cantidad de espacios
     */
    public int canEspacios(String texto) {
        return texto.replaceAll("[^\\s]", "").length();
    }

    /**
     * Elimina los espacios en blanco que aparecen en una frase
     *
     * @param texto que se desea evaluar
     * @return String sin los espacios eliminados
     */
    public String eliEspacios(String texto) {
        return texto.replaceAll("[\\s]", "");
    }

    //Poner en mayuscula la primera letra de cada palabra de una frase
    //hola mundo = Hola Mundo
    public String primeraLetra(String texto) {
        String nuevo = "";
        boolean antEsp = true;
//        "ho la mun do" = "Ho La Mun Do"
        for (int i = 0; i < texto.length(); i++) {
            if (antEsp) {
                nuevo += Character.toUpperCase(texto.charAt(i));
                antEsp = false;
            } else if (texto.charAt(i) == ' ') {
                nuevo += texto.charAt(i);
                antEsp = true;
            } else {
                nuevo += texto.charAt(i);
            }
        }
        return nuevo;
    }
    
    //Contar la cantidad de números pares en un texto
}
