/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class PruebaGarrobo {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static double leerDouble(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Double.parseDouble(sc.nextLine());
    }

    public static String leerString(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return sc.nextLine();
    }

    public static void main(String[] args) {
        Garrobo g1 = new Garrobo();
        Garrobo g2 = new Garrobo();
        final int PIN = 2390;

        String menuP = "\n  MENU PRINCIPAL\n"
                + "1. Configurar\n"
                + "2. Modo niños\n"
                + "3. Salir\n"
                + "Seleccione una opción";

        String menuC = "\n  MENU CONFIGURACIÓN\n"
                + "1. Garrobo 1\n"
                + "2. Garrobo 2\n"
                + "3. Salir\n"
                + "Seleccione una opción";

        String menuN = "\n  MENU NIÑOS\n"
                + "1. Tiempo - Garrobo 1\n"
                + "2. Tiempo - Garrobo 2\n"
                + "3. Distancia  - Garrobo 1\n"
                + "4. Distancia  - Garrobo 2\n"
                + "Seleccione una opción";

        while (true) {
            int op = leerInt(menuP);
            if (op == 3) {
                break;
            } else if (op == 1) { //Configuración de los garrobos 
                while (true) {
                    op = leerInt(menuC);
                    if (op == 3) {
                        break;
                    } else if (op == 1) {
                        g1.setNombre(leerString("Digite el nombre del garrobo"));
                        g1.setDistObs(leerDouble("Digite la distancia observada"));
                        g1.setTiemObs(leerInt("Digite tiempo observado"));
                        System.out.println(g1);
                    } else if (op == 2) {
                        g2.setNombre(leerString("Digite el nombre del garrobo"));
                        g2.setDistObs(leerDouble("Digite la distancia observada"));
                        g2.setTiemObs(leerInt("Digite tiempo observado"));
                        System.out.println(g2);
                    }
                }
            } else if (op == 2) {
                while (true) {
                    op = leerInt(menuN);
                    if (op == PIN) {
                        break;
                    } else if (op == 1) {
                        double dis = leerDouble("Distancia a recorrer");
                        double tie = g1.calcularTiempo(dis);
                        System.out.println("Hola amiguito, me llamo "
                                + g1.getNombre() + ", tardo " + tie + "s "
                                + " recorriendo " + dis + "m");
                    }
                }
            }
        }
    }
}
