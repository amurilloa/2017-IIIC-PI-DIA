/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author ALLAN
 */
public class Garrobo {

    //Atributos
    private String nombre;
    private double distObs;
    private int tiemObs;

    public Garrobo() {
    }

    public Garrobo(String nombre, double distObs, int tiemObs) {
        this.nombre = nombre;
        this.distObs = distObs;
        this.tiemObs = tiemObs;
    }

    /**
     * Calcula el tiempo que dura recorriendo x distancia
     * @param distancia double distancia a evaluar 
     * @return double tiempo de dura recorriendo dicha distancia 
     */
    public double calcularTiempo(double distancia) {
        double vel = distObs / tiemObs;
        return distancia / vel;
    }

    /**
     * Calcula la distancia que puede recorrer el garrobo en x tiempo
     * @param tiempo Int tiempo a evaluar
     * @return double distancia que puede recorrer en ese tiempo
     */
    public double calcularDistancia(int tiempo) {
        double vel = distObs / tiemObs;
        return vel * tiempo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getDistObs() {
        return distObs;
    }

    public void setDistObs(double distObs) {
        this.distObs = distObs;
    }

    public int getTiemObs() {
        return tiemObs;
    }

    public void setTiemObs(int tiemObs) {
        this.tiemObs = tiemObs;
    }

    @Override
    public String toString() {
        return "Garrobo{" + "nombre=" + nombre + ", distObs=" + distObs + ", tiemObs=" + tiemObs + '}';
    }
}
