/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Gasolinera {

    private double precioXLitro;

    public Gasolinera() {
        precioXLitro = 632;
    }

    public Gasolinera(double precioXLitro) {
        this.precioXLitro = precioXLitro;
    }

    /**
     * 
     * @param galones
     * @return 
     */
    public double cobro(double galones) {
        double valor = galones * 3.78541 * precioXLitro;
        return Math.round(valor * 100) / 100;
    }

    public double getPrecioXLitro() {
        return precioXLitro;
    }

    public void setPrecioXLitro(double precioXLitro) {
        this.precioXLitro = precioXLitro;
    }

    @Override
    public String toString() {
        return "Gasolinera{" + "precioXLitro=" + precioXLitro + '}';
    }

}
