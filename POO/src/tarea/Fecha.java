/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Fecha {

    private int dia;
    private int mes;
    private int anno;

    public Fecha() {
        dia = 1;
        mes = 1;
        anno = 2000;
    }
    
    public Fecha(int dia, int mes, int anno) {
        if (validarFecha(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {
            this.dia = 1;
            this.mes = 1;
            this.anno = 2000;
        }
    }

    /**
     * 
     * @param dia
     * @param mes
     * @param anno 
     */
    public void modificarFecha(int dia, int mes, int anno) {
        if (validarFecha(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {
            this.dia = 1;
            this.mes = 1;
            this.anno = 2000;
        }
    }

    /**
     * 
     * @return 
     */
    public String getFecha() {
        String d = dia < 10 ? "0" + dia : String.valueOf(dia);
        String m = mes < 10 ? "0" + mes : String.valueOf(mes);
        return d + "/" + m + "/" + anno;
    }

    /**
     * 
     * @return 
     */
    public String getFechaL() {
        return dia + " de " + mesTexto(mes) + " de " + anno;
    }

    /**
     * 
     * @param dia
     * @param mes
     * @param anno
     * @return 
     */
    private boolean validarFecha(int dia, int mes, int anno) {

        if (dia <= 0 || dia > 31) {
            return false;
        } else if (mes <= 0 || mes > 12) {
            return false;
        } else if (mes == 2 && esBisiesto(anno) && dia >= 30) {
            return false;
        } else if (mes == 2 && !esBisiesto(anno) && dia >= 29) {
            return false;
        } else if (dia > 30 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)) {
            return false;
        }

        return true;
    }

    /**
     * 
     * @param anno
     * @return 
     */
    private boolean esBisiesto(int anno) {
        if (anno % 4 == 0) {
            if (anno % 100 == 0) {
                return anno % 400 == 0;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param mes
     * @return 
     */
    private String mesTexto(int mes) {
        switch (mes) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
            default:
                return "Error";
        }
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    @Override
    public String toString() {
        return "Fecha{" + "dia=" + dia + ", mes=" + mes + ", anno=" + anno + '}';
    }
}
