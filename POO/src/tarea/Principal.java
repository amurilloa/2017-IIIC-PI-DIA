/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {

        Llamada nueva = new Llamada(5);
        System.out.println("$" + nueva.costo());

        /*
        Mesa m1 = new Mesa();
        while (true) {
            int plato = Util.leerInt("\n" + m1.menu() + "\n7.Finalizar Orden\n Seleccione una opción");
            if (plato == 7) {
                break;
            }
            int cantidad = Util.leerInt("Cantidad a ordenar");
            m1.pedirOrden(plato, cantidad);
        }

        System.out.println("Monto a Cancelar: $" + m1.calcularCuenta());
        System.out.println("\n\n");
        Circunferencia rueda = new Circunferencia(10.2);
        Circunferencia moneda = new Circunferencia(1.4);

        Rectangulo pared = new Rectangulo();
        Rectangulo ventana = new Rectangulo();

        Fecha f = new Fecha(5, 6, 1988);
        System.out.println(f.getFecha());
        f.modificarFecha(29, 2, 2000);
        System.out.println(f.getFechaL());

        Gasolinera g = new Gasolinera();
        System.out.println(g.cobro(1.1));

        String menuE = "\n    MENU PRACTICA\n"
                + "1. Circunferencia\n"
                + "2. Rectangulo\n"
                + "8. Salir\n"
                + "Seleccione una opción";

        String menuC = "\n    MENU CIRCUNFERENCIA\n"
                + "1. Área de Rueda\n"
                + "2. Área de Moneda\n"
                + "3. Perímetro de Rueda\n"
                + "4. Perímetro de Moneda\n"
                + "5. Volver\n"
                + "Seleccione una opción";

        String menuR = "\n    MENU RECTANGULO\n"
                + "1. Datos de la pared\n"
                + "2. Datos de la ventana\n"
                + "3. Tiempo estimado\n"
                + "4. Volver\n"
                + "Seleccione una opción";
        SALIR:
        while (true) {
            int op = Util.leerInt(menuE);
            switch (op) {
                case 8: //Salir
                    System.out.println("Gracias por utilizar la aplicación");
                    break SALIR;
                case 1: //Circunferencia
                    SALIR_C:
                    while (true) {
                        op = Util.leerInt(menuC);
                        switch (op) {
                            case 5:
                                break SALIR_C;
                            case 1:
                                // Util.mostrar("Área de la rueda es: " + rueda.calcularArea());
                                System.out.println("Área de la rueda es: " + rueda.calcularArea());
                                break;
                            case 2:
                                System.out.println("Área de la moneda es: " + moneda.calcularArea());
                                break;
                            case 3:
                                System.out.println("Perímetro de la rueda es: " + rueda.calcularPerimetro());
                                break;
                            case 4:
                                System.out.println("Perímetro de la moneda es: " + moneda.calcularPerimetro());
                                break;
                            default:
                                System.out.println("Opción inválida intente nuevamente");
                        }
                    }
                    break;
                case 2: //Rectangulo
                    SALIR_R:
                    while (true) {
                        op = Util.leerInt(menuR);
                        switch (op) {
                            case 4:
                                break SALIR_R;
                            case 1:
                                pared.setLargo(Util.leerDouble("Largo de la pared"));
                                pared.setAncho(Util.leerDouble("Ancho de la pared"));
                                break;
                            case 2:
                                ventana.setLargo(Util.leerDouble("Largo de la ventana"));
                                ventana.setAncho(Util.leerDouble("Ancho de la ventana"));
                                break;
                            case 3:
                                double m2p = pared.calcularArea() - ventana.calcularArea();
                                int m = (int) m2p * 10;
                                int h = m / 60;
                                m = m % 60;
                                System.out.println(h + "h:" + m + "m");
                                break;
                            default:
                                System.out.println("Opción inválida intente nuevamente");
                        }
                    }
                    break;
                default:
                    System.out.println("Opción inválida intente nuevamente");
            }
        }
         */
    }
}
