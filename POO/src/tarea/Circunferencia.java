/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Circunferencia {

    private double radio;

    public Circunferencia() {
    }

    public Circunferencia(double radio) {
        this.radio = radio;
    }

    /**
     * Calcula el area de la circunferencia
     * @return double Area de la circunferencia
     */
    public double calcularArea() {
        double area = Math.PI * Math.pow(radio, 2);
        return Math.round(area * 100) / 100;
    }

    /**
     * Calcula el perimetro de la circunferencia
     * @return double Perimetro de la circunferencia
     */
    public double calcularPerimetro() {
        double perimetro = 2 * Math.PI * radio;
        return Math.round(perimetro * 100) / 100;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    @Override
    public String toString() {
        return "Circunferencia{" + "radio=" + radio + '}';
    }
}
