/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class Util {
    
    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }
    
    public static int leerIntJOP(String mensaje) {
        return Integer.parseInt(JOptionPane.showInputDialog(mensaje));
    }
    
    public static void mostrar(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }
    
    public static double leerDouble(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Double.parseDouble(sc.nextLine());
    }
    
    public static String leerString(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return sc.nextLine();
    }
}
