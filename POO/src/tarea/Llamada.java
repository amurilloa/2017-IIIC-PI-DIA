/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Llamada {

    private int duracion;

    public Llamada() {
    }

    public Llamada(int duracion) {
        if (duracion > 0) {
            this.duracion = duracion;
        }
    }

    public int costo() {
        int minutos = duracion;
        int costo = 0;

        if (minutos > 0) {
            costo += 5;
            minutos -= 3;
        }

        if (minutos > 0) {
            costo += minutos * 3;
        }
        return costo;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        if (duracion > 0) {
            this.duracion = duracion;
        } else {
            this.duracion = 0;
        }
    }

}
