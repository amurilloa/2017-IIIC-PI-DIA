/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Mesa {

    private Producto p1; //TODO pasar a un arreglo
    private Producto p2;
    private Producto p3;
    private Producto p4;
    private Producto p5;
    private Producto p6;

    public Mesa() {
        p1 = new Producto("Hamburguesa Sencilla", 15, 0);
        p2 = new Producto("Hamburguesa con queso", 18, 0);
        p3 = new Producto("Hamburguesa especial", 20, 0);
        p4 = new Producto("Papas fritas", 8, 0);
        p5 = new Producto("Refresco", 5, 0);
        p6 = new Producto("Postre", 6, 0);
    }

    public void pedirOrden(int plato, int cantidad) {
        switch (plato) {
            case 1:
                p1.setCantidad(cantidad);
                break;
            case 2:
                p2.setCantidad(cantidad);
                break;
            case 3:
                p3.setCantidad(cantidad);
                break;
            case 4:
                p4.setCantidad(cantidad);
                break;
            case 5:
                p5.setCantidad(cantidad);
                break;
            case 6:
                p6.setCantidad(cantidad);
                break;
        }
    }

    public int calcularCuenta() {
        return p1.getTotal() + p2.getTotal()
                + p3.getTotal() + p4.getTotal()
                + p5.getTotal() + p6.getTotal();
    }

    public String menu() {
        String menu = "1. " + p1.getDescripcion() + " \t($" + p1.getPrecio() + ") \t " + p1.getCantidad() + "\n"
                + "2. " + p2.getDescripcion() + " \t($" + p2.getPrecio() + ") \t " + p2.getCantidad() + "\n"
                + "3. " + p3.getDescripcion() + " \t($" + p3.getPrecio() + ") \t " + p3.getCantidad() + "\n"
                + "4. " + p4.getDescripcion() + " \t\t($" + p4.getPrecio() + ") \t " + p4.getCantidad() + "\n"
                + "5. " + p5.getDescripcion() + "\t\t\t($" + p5.getPrecio() + ") \t " + p5.getCantidad() + "\n"
                + "6. " + p6.getDescripcion() + "\t\t\t($" + p6.getPrecio() + ") \t " + p6.getCantidad() + "";

        return menu;
    }

}
