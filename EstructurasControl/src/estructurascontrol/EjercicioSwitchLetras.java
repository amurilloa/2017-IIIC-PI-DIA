/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioSwitchLetras {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite una letra a-e: ");
        char letra = sc.nextLine().charAt(0);
        switch (letra) {
            case 'a':
            case 'A':
                System.out.println("Excelente");
                break;
            case 'b':
            case 'B':
                System.out.println("Bueno");
                break;
            case 'c':
            case 'C':
                System.out.println("Regular");
                break;
            case 'd':
            case 'D':
                System.out.println("Malo");
                break;
            case 'e':
            case 'E':
                System.out.println("Pésimo");
                break;
        }

    }
}
