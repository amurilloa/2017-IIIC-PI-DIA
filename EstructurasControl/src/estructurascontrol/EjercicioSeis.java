/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioSeis {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite el monto a retirar: ");
        int monto = Integer.parseInt(sc.nextLine());

        int moneda = 500;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " Billetes de " + moneda);
            monto %= moneda; //monto = monto%moneda;
        }
        moneda = 200;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " Billetes de " + moneda);
            monto %= moneda; //monto = monto%moneda;
        }
        moneda = 100;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " Billetes de " + moneda);
            monto %= moneda; //monto = monto%moneda;
        }
        moneda = 50;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " Billetes de " + moneda);
            monto %= moneda; //monto = monto%moneda;
        }
        moneda = 20;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " Billetes de " + moneda);
            monto %= moneda; //monto = monto%moneda;
        }
        moneda = 10;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " Billetes de " + moneda);
            monto %= moneda; //monto = monto%moneda;
        }
        moneda = 5;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " Billetes de " + moneda);
            monto %= moneda; //monto = monto%moneda;
        }
        moneda = 2;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " Monedas de " + moneda);
            monto %= moneda; //monto = monto%moneda;
        }
        moneda = 1;
        if (monto >= moneda) {
            System.out.println(monto + " Monedas de " + moneda);
            monto %= moneda; //monto = monto%moneda;
        }
    }
}
