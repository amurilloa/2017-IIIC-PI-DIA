/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioTres {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //Salida
        double costoTotal = 0;
        //Datos entrada
        System.out.print("Digite la cantidad de ejes: ");
        int numeroEjes = Integer.parseInt(sc.nextLine());
        System.out.print("Digite la cantidad de pasajeros: ");
        int cantPasajeros = Integer.parseInt(sc.nextLine());
        System.out.print("Digite el costo del vehículo: ");
        double costo = Double.parseDouble(sc.nextLine());
        double imp = costo * 0.01;
        double impE = 0;
        double impP = 0;

        if (cantPasajeros < 20) {
            impP = imp * 0.01;
        } else if (cantPasajeros >= 20 && cantPasajeros <= 60) {
            impP = imp * 0.05;
        } else {
            impP = imp * 0.08;
        }

        if (numeroEjes == 2) {
            impE = imp * 0.05;
        } else if (numeroEjes == 3) {
            impE = imp * 0.10;
        } else if (numeroEjes > 3) {
            impE = imp * 0.15;
        }

        costoTotal = costo + imp + impE + impP;

        System.out.println("El costo del vehículo: " + costoTotal);
    }

}
