/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjemploWhile {

    public static void main(String[] args) {
//        int cont = 1;
//        while (cont <= 10) {
//            System.out.println(cont);
//            cont++;
//        }
//
//        cont = 10;
//        while (cont > 0) {
//            System.out.println(cont);
//            cont--;
//        }

//        int a = 1;
//
//        while (true) {
//            System.out.println(a);
//            if (a == 3) {
//                break;
//            }
//            a++;
//        }
        Scanner sc = new Scanner(System.in);
//        System.out.print("Digite un número: ");
//        int tabla = Integer.parseInt(sc.nextLine());
//        int con = 0;
//        while (con <= 9) {
//            System.out.println(tabla + "x" + con + "=" + (tabla * con));
//            con++;
//        }
        String menu = "MENU PRINCIPAL\n"
                + "+ Sumar\n"
                + "- Restar\n"
                + "* Multiplicar\n"
                + "/ Dividir\n"
                + "s Salir\n"
                + "Seleccione una opción: ";
        while (true) {
            System.out.println(menu);
            char op = sc.nextLine().charAt(0);
            if (op == 's' || op == 'S') {
                System.out.println("Gracias por utilizar nuestra aplicación. ");
                break;
            }

            System.out.print("Digite el número 1: ");
            int n1 = Integer.parseInt(sc.nextLine());
            System.out.print("Digite el número 2: ");
            int n2 = Integer.parseInt(sc.nextLine());

            switch (op) {
                case '+':
                    System.out.println(n1 + "+" + n2 + "=" + (n1 + n2));
                    break;
                case '-':
                    System.out.println(n1 + "-" + n2 + "=" + (n1 - n2));
                    break;
                case '*':
                    System.out.println(n1 + "*" + n2 + "=" + (n1 * n2));
                    break;
                case '/':
                    if (n2 != 0) {
                        System.out.println(n1 + "/" + n2 + "=" + (n1 / (double) n2));
                    } else {
                        System.out.println("Error, división por 0");
                    }
                    break;
                default:
                    System.out.println("Opción Inválida");
            }
        }
    }
}
