/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioSwitchNumeros {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite un número entre 0-99: ");
        int num = Integer.parseInt(sc.nextLine());
        int uni = num % 10;
        int dec = num / 10;

        switch (dec) {
            case 0:
                break;
            case 1:
                switch (uni) {
                    case 0:
                        System.out.println("Diez");
                        uni = -1;
                        break;
                    case 1:
                        System.out.println("Once");
                        uni = -1;
                        break;
                    case 2:
                        System.out.println("Doce");
                        uni = -1;
                        break;
                    case 3:
                        System.out.println("Trece");
                        uni = -1;
                        break;
                    case 4:
                        System.out.println("Catorce");
                        uni = -1;
                        break;
                    case 5:
                        System.out.println("Quince");
                        uni = -1;
                        break;
                    case 6:
                        System.out.println("Dieciseis");
                        uni = -1;
                        break;
                    case 7:
                        System.out.println("Diecisiete");
                        uni = -1;
                        break;
                    case 8:
                        System.out.println("Dieciocho");
                        uni = -1;
                        break;
                    case 9:
                        System.out.println("Diecinueve");
                        uni = -1;
                        break;
                }
                break;
            case 2:
                System.out.print("Veinte");
                break;
            case 3:
                System.out.print("Treinta");
                break;
            case 4:
                System.out.print("Cuarenta");
                break;
            case 5:
                System.out.print("Cincuenta");
                break;
            case 6:
                System.out.print("Sesenta");
                break;
            case 7:
                System.out.print("Setenta");
                break;
            case 8:
                System.out.print("Ochenta");
                break;
            case 9:
                System.out.print("Noventa");
                break;

        }
        if (uni > 0) {
            System.out.print(" y ");
        }
        switch (uni) {
            case 0:
                if (dec == 0) {
                    System.out.println("Cero");
                }
                break;
            case 1:
                System.out.println("Uno");
                break;
            case 2:
                System.out.println("Dos");
                break;
            case 3:
                System.out.println("Tres");
                break;
            case 4:
                System.out.println("Cuatro");
                break;
            case 5:
                System.out.println("Cinco");
                break;
            case 6:
                System.out.println("Seis");
                break;
            case 7:
                System.out.println("Siete");
                break;
            case 8:
                System.out.println("Ocho");
                break;
            case 9:
                System.out.println("Nueve");
                break;
        }

    }

}
