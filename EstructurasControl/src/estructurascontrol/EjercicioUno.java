/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioUno {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite un número: ");
        int n = Integer.parseInt(sc.nextLine());
        if (n >= 0) {
            System.out.println("Positivo");
        } else {
            System.out.println("Negativo");
        }

        if (n % 5 == 0) {
            System.out.println("Divisible entre 5");
        }
    }
}
