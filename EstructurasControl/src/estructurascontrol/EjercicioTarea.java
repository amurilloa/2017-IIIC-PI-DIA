/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioTarea {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //Datos de entrada 
        System.out.print("El precio del crucero: ");
        int precio = Integer.parseInt(sc.nextLine());
        System.out.print("Cantidad de Tiquetes: ");
        int cantPers = Integer.parseInt(sc.nextLine());
        double desc = 0;

        if (cantPers == 1) {
            System.out.print("Edad del viajero: ");
            int edad = Integer.parseInt(sc.nextLine());
            if (edad >= 18 && edad <= 30) {
                desc = precio * 0.078;
            } else if (edad > 30) {
                desc = precio * 0.1;
            }
        } else if (cantPers == 2) {
            desc = precio * 0.115;
        } else if (cantPers > 3) {
            desc = precio * 0.15;
        }

        System.out.println("Precio: $" + (precio - desc) * cantPers);
        if (desc > 0) {
            System.out.println("Descuento: $" + desc * cantPers);
        }
    }
}
