/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructurasControl {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Digite el primer número: ");
        int n1 = Integer.parseInt(sc.nextLine());
        System.out.print("Digite el segundo número: ");
        int n2 = Integer.parseInt(sc.nextLine());

        System.out.println("Digite 1 para sumar y otro para restar: ");
        String datos = sc.nextLine();// "10"
        int op = Integer.parseInt(datos);

        if (op == 1) {
            int res = n1 + n2;
            System.out.println("El resultado: " + res);
        } else {
            int res = n1 - n2;
            System.out.println("El resultado: " + res);
        }
    }
}
