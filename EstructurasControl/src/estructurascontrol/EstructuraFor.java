/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructuraFor {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //        System.out.println("Digite un número: ");
        //        int tabla = Integer.parseInt(sc.nextLine());
        //        for (int i = 0; i <= 10; i++) {
        //            System.out.println(tabla + "x" + i + "=" + (tabla * i));
        //        }
        //        for (int i = 1; i <= 1000; i++) {
        //            if (i % 7 == 0) {
        //                System.out.println(i);
        //            }
        //        }
        int ale = (int) (Math.random() * 20) + 1;
        System.out.println(ale);

        for (int i = 1; i < 6; i++) {
            System.out.println(i + "/" + 5 + "intentos");
            System.out.println("Digite un número 1-20: ");
            int res = Integer.parseInt(sc.nextLine());

            if (res == ale) {
                System.out.println("Felicidades!!!");
                break;
            } else if (i == 5) {
                System.out.println("Lo siento, no hay más intentos");
            } else if (res > ale) {
                System.out.println("Digite un número menor");
            } else if (res < ale) {
                System.out.println("Digite un número mayor");
            }
        }
    }
}
