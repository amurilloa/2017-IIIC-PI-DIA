/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioCuatro {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite la nota: ");
        double nota = Double.parseDouble(sc.nextLine());

        if (nota >= 90) {
            System.out.println("Sobresaliente");
        } else if (nota >= 80) {
            System.out.println("Notable");
        } else if (nota >= 70) {
            System.out.println("Bien");
        } else {
            System.out.println("Insuficiente");
        }

    }
}
