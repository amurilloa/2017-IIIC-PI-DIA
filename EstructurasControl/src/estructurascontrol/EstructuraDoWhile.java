/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructuraDoWhile {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double sumatoria = 0;
        int edad = 0;
        do {
            System.out.print("Digite su edad: ");
            edad = Integer.parseInt(sc.nextLine());
            sumatoria += edad;
        } while (edad != 0);
        System.out.println(sumatoria);
    }
}
