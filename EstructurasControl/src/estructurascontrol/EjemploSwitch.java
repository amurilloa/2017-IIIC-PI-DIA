/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjemploSwitch {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite un número de día 1-7: ");
        int dia = Integer.parseInt(sc.nextLine());
        switch (dia) {
            case 1:
                System.out.println("Domingo");
                break;
            case 2:
                System.out.println("Lunes");
                break;
            case 3:
                System.out.println("Martes");
                break;
            case 4:
                System.out.println("Miércoles");
                break;
            case 5:
                System.out.println("Jueves");
                break;
            case 6:
                System.out.println("Viernes");
                break;
            case 7:
                System.out.println("Sábado");
                break;
            default:
                System.out.println("Día incorrecto");
        }

        System.out.print("Digite un número de mes 1-12: ");
        int mes = Integer.parseInt(sc.nextLine());

        switch (mes) {
            case 1:
                System.out.println("Enero");
                break;
            case 2:
                System.out.println("Febrero");
                break;
            case 3:
                System.out.println("Marzo");
                break;
            case 4:
                System.out.println("Abril");
                break;
            case 5:
                System.out.println("Mayo");
                break;
            case 6:
                System.out.println("Junio");
                break;
            case 7:
                System.out.println("Julio");
                break;
            case 8:
                System.out.println("Agosto");
                break;
            case 9:
                System.out.println("Septiembre");
                break;
            case 10:
                System.out.println("Octubre");
                break;
            case 11:
                System.out.println("Noviembre");
                break;
            case 12:
                System.out.println("Diciembre");
                break;
            default:
                System.out.println("Mes incorrecto");
        }

        System.out.print("Digite un número de lineas: ");
        int op = Integer.parseInt(sc.nextLine());

        switch (op) {
            case 5:
                System.out.println("********");
            case 4:
                System.out.println("********");
            case 3:
                System.out.println("********");
            case 2:
                System.out.println("********");
            case 1:
                System.out.println("********");
        }

        System.out.print("Digite número 1: ");
        int num1 = Integer.parseInt(sc.nextLine());
        System.out.print("Digite número 2: ");
        int num2 = Integer.parseInt(sc.nextLine());

        System.out.print("Digite un operador (+,-,/,*): ");
        char ope = sc.nextLine().charAt(0);
        switch (ope) {
            case '+':
                System.out.println(num1 + "+" + num2 + "=" + (num1 + num2));
                break;
            case '-':
                System.out.println(num1 + "-" + num2 + "=" + (num1 - num2));
                break;
            case '*':
                System.out.println(num1 + "*" + num2 + "=" + (num1 * num2));
                break;
            case '/':
                if (num2 == 0) {
                    System.out.println("Error, división por 0");
                } else {
                    double res = num1 / (double) num2;
                    res = Math.round(res * 100) / 100.0;//Redondeo a 2 decimales
                    System.out.println(num1 + "/" + num2 + "=" + res);
                }
                break;
        }

    }
}
