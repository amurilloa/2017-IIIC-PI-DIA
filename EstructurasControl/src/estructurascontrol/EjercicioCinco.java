/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioCinco {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite la cantidad de Partituras: ");
        int canPar = Integer.parseInt(sc.nextLine());
        System.out.print("Digite la cantidad de Canciones: ");
        int canCan = Integer.parseInt(sc.nextLine());

        if (canCan >= 7 && canCan <= 10) {
            if (canPar == 0) {
                System.out.println("Músico Naciente");
            } else if (canPar >= 1 && canPar <= 5) {
                System.out.println("Músico Estelar");
            } else {
                System.out.println("Músico en formación");
            }
        } else if (canCan > 10 && canPar > 5) {
            System.out.println("Músico consagrado");
        } else {
            System.out.println("Músico en formación");
        }

        if (canCan >= 7 && canCan <= 10 && canPar == 0) {
            System.out.println("Músico Naciente");
        } else if (canCan >= 7 && canCan <= 10 && canPar >= 1 && canPar <= 5) {
            System.out.println("Músico Estelar");
        } else if (canCan > 10 && canPar > 5) {
            System.out.println("Músico consagrado");
        } else {
            System.out.println("Músico en formación");
        }

    }
}
